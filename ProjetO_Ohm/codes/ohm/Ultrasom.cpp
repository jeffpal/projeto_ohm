/**
 * Corpo da classe Ultrasom
 */

#include <Arduino.h>
#include "Ultrasom.h"

Ultrasom::Ultrasom(int pinoGAT, int pinoECO) : Ultrasonic(pinoGAT, pinoECO)
{
    pino_gatilho = pinoGAT;
    pino_eco = pinoECO;
}

int Ultrasom::Distancia()
{
    return Ranging(CM);
}
