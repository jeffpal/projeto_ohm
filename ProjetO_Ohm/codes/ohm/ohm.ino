//Projeto Ohm - Código de controle do robô Ohm
//o código tem duas funções principais. Uma para controlar o robÔ pelo Android e outra para que o robô haja de forma autônoma
//se o usuário enviar o comando de ativação para controle pelo Android o robô ficara aguardando por comandos de controle 
//caso contrrário, ele agirá de forma autônoma para localizar e atacar outros robô em um ringiue de sumô

//Autor: Jefferson Ferreira Palheta
//Email: jefferson.e.etc@gmail.com





//declaração e definição dos pinos utilizados pelos sensores e motores
#define trig 9
#define echo 10
#define line_sensor 11
#include "Ultrasom.h"
Ultrasom us(trig,echo);
int diff;
int estate;
int reference_color; //variável que recebe o valor de "cor" da superficie lida pelo sensor de linha 
int command; //variável que recebe o valor do comando envianndo pelo Android
int line; // valor atual lido pelo sensor de linha
int som; // valor da leitura do sonar
int le; //valor retornado pela função fulga

//função responsável por movimentar o robô para fente
void frente() {
    desliga(); //desliga todos os motores
    digitalWrite (2, HIGH); //liga o pino 2 
    digitalWrite (4, HIGH);//liga o pino 4
    estate=1;
}
//______________________________________________________________________

//função responsável por movimentar o robô para trás
void tras() {
    desliga();
    digitalWrite (3, HIGH);//liga o pino 3
    digitalWrite (8, HIGH);//liga o pino 8
    estate=1;
}
//______________________________________________________________________


//função responsável por movimentar o robô para a esuqerda
void esquerda() {
    desliga();
    digitalWrite (3, HIGH);//liga o pino 3 
    digitalWrite (4, HIGH);//liga o pino 4
    estate=1;
  }
//______________________________________________________________________


//função responsável por movimentar o robô para direita
void direita() {
    desliga();
    digitalWrite (2, HIGH);
    digitalWrite (8, HIGH);
    estate=1;
  }
//______________________________________________________________________

//função responsável por desligar todos os motores
void desliga() {
    digitalWrite (2, LOW); //desliga o pino 2
    digitalWrite (3, LOW); //desliga o pino 2
    digitalWrite (4, LOW); //desliga o pino 2
    digitalWrite (8, LOW); //desliga o pino 2
    estate=0;
  }
//______________________________________________________________________


//está função faz a leitura do sensor de linha e retorna o valor lido atráves da variável diff
int read() {
  pinMode( line_sensor, OUTPUT );
  digitalWrite( line_sensor, HIGH );
  delayMicroseconds(10);
  pinMode( line_sensor, INPUT );
  long time = micros();
  while (digitalRead(line_sensor) == HIGH && micros() - time < 1000);
  diff = micros() - time;
  return diff;
}

//______________________________________________________________________

//função de leitura do sensor ultra sônico que utiliza outras bibliotecas e funções especificas para o sonar e estão presentes no pacote de docomentação do robô
int sonar()
{
  //retorna o valor específico obtido na leitura 
  return(us.Distancia());  
}

//______________________________________________________________________

//está função recebe o valor do sensor de linha e retorna 0 ou 1 conforme o parâmetro de controle aplicado
int fulga(int line){
  
  //se oito vezes o valor lido no sensor de linha for menor que 3000, então a função retornará 1, caso contrário retornará 0
  if(8*line<3000)//reference_color)
  {
    return 1;
  }
  
  else
  {
    return 0;
  }
}

//______________________________________________________________________

//está função é utilizada somente quando o robô está no modo de controle via Android

void control()
{
//a variável command recebe o valor lido pa porta serial que é justamente o valor de comando enviado pelo aparelho via bluetooth  
command = Serial.read();

if (command<=4 && command >=1)
{
  
    //FRENTE  
    if (command==3)
    {
      if (estate==0)
      {
      frente();
      }
      else if (estate==1)
      {
       desliga();
      }
    }


    //TRAS
    else if (command==4)
    {
      
      if (estate==0)
      {
      tras();
      }
      else if (estate==1)
      {
      desliga();
      }
    }


    //DIREITA
    else if (command==2)
    {
      if (estate==0)
      {
      direita();
      }
      else if (estate==1)
      {
      desliga();
      }
    }

//ESQUERDA
    else if (command==1)
    {
      if (estate==0)
      {
      esquerda();
      }
      else if (estate==1)
      {
      desliga();
      }
    }

}


else if(command==0)
{
desliga();
return loop();
}
return control();
}
//______________________________________________________________________

void setup() {
  Serial.begin(9600);
  delay(10000);
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(8, OUTPUT);
 
  reference_color=read(); 
  Serial.print("referencia: ");
  Serial.println(reference_color);


}
//______________________________________________________________________

void loop() {

  command = Serial.read();
  Serial.println("SERIAL: ");
  Serial.println(command); 
  
  //se o comando enviado pelo dispositivo Android for o comando para ativação do modo de controle
  //então se chamada a função "control" e esta será executada até que o comando de desativação do modo de controle seja executado novamente
  if (command==0)
  {
     desliga();
     control();     
  }
     
 //caso não seja enviado comando de ativação para o modo de controle o robô funcionará no modo autônomo      
  else
  {  
   line=read();
   som=sonar();
   le=fulga(line);
  
   Serial.print("linha:");
   Serial.println(line);
   Serial.print("Sonar: ");
   Serial.println(som);
   

       if(le==0)
      {   //enquanto houver algum objeto dentro de uma distância de 50 cm o robô atacará indo para frente 
          while(som>0 & som<50 & le==0)
          {  
          frente();
          delay(500);
          som=sonar();
          }  
         
         frente();
      }
    
      else if(le==1)
      {
      tras();
      delay(2000);
      direita();
      delay(1000); 
      }

    }
}
//______________________________________________________________________



