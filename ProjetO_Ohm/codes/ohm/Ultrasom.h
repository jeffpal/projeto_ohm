/**
 * Cabeçalho da classe Ultrasom
 */

#include <Arduino.h>
#include "Ultrasonic.h"

#ifndef Ultrasom_h
#define Ultrasom_h

class Ultrasom : public Ultrasonic
{
  public:
    Ultrasom(int pinoGAT, int pinoECO);
    int Distancia();

  private:
    int pino_gatilho;
    int pino_eco;
};

#endif
